//
//  CatalogModel.m
//  CatalogTestApp
//
//  Created by Eugene Pavlyuk on 11/19/13.
//  Copyright (c) 2013 Eugene Pavlyuk. All rights reserved.
//

#import "CatalogModel.h"
#import "ConnectionManager.h"
#import "ItemParser.h"
#import "DataManager.h"
#import "Item.h"

@implementation CatalogModel
{
    NSDate *lastUpdatedDate;
    NSInteger currentPage;
}

- (id)init
{
    self = [super init];
        
    if(self)
    {
        currentPage = 1;
        
        self.reloading = YES;
        [self resetModel];
        
        NSMutableArray *items = [[[DataManager sharedInstance] allRowsForEntity:kItemEntity] mutableCopy];
                
        [self.objects addObjectsFromArray:items];
    }
    
    return self;
}

- (BOOL)isLoading
{
    return [super isLoading];
}

- (BOOL)canLoadMore
{
    return YES;
}

- (BOOL)didLoadFirstPage
{
    return (![self isLoading] && [self.objects count]/* <= [pager[kItemOffsetKey] intValue]*/);
}

- (NSDate*)lastUpdatedDate
{
    return lastUpdatedDate;
}

- (void)resetModel
{
    [super resetModel];
    
    currentPage = 1;
}

- (void)loadItemsWithInfo:(NSDictionary*)info
          completionBlock:(modelCompletionBlock)completionBlock
             failureBlock:(modelFailureBlock)failureBlock
{
    isLoading = YES;
    
    NSMutableDictionary *parameters = [info mutableCopy];
    
    [parameters setObject:[NSString stringWithFormat:@"%i", currentPage] forKey:kPageKey];

    
    [[ConnectionManager sharedInstance] loadItemsWithInfo:parameters
                                          completionBlock:^(id object) {
        
        isLoading = NO;
                                              
        ItemParser *feedParser = [[ItemParser alloc] init];
        
        if (self.reloading)
        {
            [[DataManager sharedInstance] removeAllForEntity:kItemEntity];
        }
                                              
        NSArray *responseObjects = [feedParser parseResponse:object];
        
        if (self.reloading)
        {
            [self resetModel];
            
            self.reloading = NO;
        }
                 
        if (feedParser.success)
        {
          currentPage++;
          
          [self.objects addObjectsFromArray:responseObjects];
          
          if (completionBlock && responseObjects)
          {
              completionBlock();
          }
          else
          {
              failureBlock();
          }
        }
        else
        {
            failureBlock();
        }
                                    
    } failureBlock:^{
        
        isLoading = NO;
        
        if (failureBlock)
        {
            failureBlock();
        }
    }];
}

@end
