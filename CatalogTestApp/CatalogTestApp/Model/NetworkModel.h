//
//  NetworkModel.h
//  CatalogTestApp
//
//  Created by Eugene Pavlyuk on 11/19/13.
//  Copyright (c) 2013 Eugene Pavlyuk. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^modelCompletionBlock) ();
typedef void (^modelFailureBlock) ();

@interface NetworkModel : NSObject
{
    BOOL isLoading;
}

@property (nonatomic, readonly) NSMutableArray *objects;

- (BOOL)isLoading;
- (BOOL)isLoadingFirstPage;
- (BOOL)didLoadFirstPage;
- (BOOL)canLoadMore;
- (NSDate*)lastUpdatedDate;

- (void)resetModel;

@end
