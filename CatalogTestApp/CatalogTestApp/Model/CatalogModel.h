//
//  CatalogModel.h
//  CatalogTestApp
//
//  Created by Eugene Pavlyuk on 11/19/13.
//  Copyright (c) 2013 Eugene Pavlyuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkModel.h"

@interface CatalogModel : NetworkModel

@property (nonatomic, assign) BOOL reloading;

- (void)loadItemsWithInfo:(NSDictionary*)info
          completionBlock:(modelCompletionBlock)completionBlock
             failureBlock:(modelFailureBlock)failureBlock;

@end
