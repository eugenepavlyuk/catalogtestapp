//
//  NetworkModel.m
//  CatalogTestApp
//
//  Created by Eugene Pavlyuk on 11/19/13.
//  Copyright (c) 2013 Eugene Pavlyuk. All rights reserved.
//

#import "NetworkModel.h"
#import "ConnectionManager.h"

@implementation NetworkModel
{
    NSMutableArray *objects;
}

@synthesize objects;

- (id)init
{
    self = [super init];
    
    if(self)
    {
        objects = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void)dealloc
{
    DLog(@"%@ dealloc", NSStringFromClass([self class]));
}

- (BOOL)isLoading
{
    return isLoading;
}

- (BOOL)canLoadMore
{
    return NO;
    // override in subclass
}

- (BOOL)isLoadingFirstPage
{
    return ([self isLoading] && [self.objects count] == 0);
}

- (BOOL)didLoadFirstPage
{
    return NO;
    // override in subclass
}

- (NSDate*)lastUpdatedDate
{
    return [NSDate date];
    // override in subclass
}

- (void)resetModel
{
    [objects removeAllObjects];
}

@end
