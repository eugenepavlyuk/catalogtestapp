//
//  ItemParser.h
//  CatalogTestApp
//
//  Created by Eugene Pavlyuk on 11/19/13.
//  Copyright (c) 2013 Eugene Pavlyuk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ItemParser : NSObject

@property (nonatomic, readonly, assign) BOOL success;

- (NSArray*)parseResponse:(NSData*)response;

@end
