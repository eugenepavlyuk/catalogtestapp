//
//  ItemParser.m
//  CatalogTestApp
//
//  Created by Eugene Pavlyuk on 11/19/13.
//  Copyright (c) 2013 Eugene Pavlyuk. All rights reserved.
//

#import "ItemParser.h"
#import "DataManager.h"
#import "Item.h"

@interface ItemParser()

@end 


@implementation ItemParser
{
    BOOL _success;
}


- (NSArray*)parseResponse:(NSData*)response
{
    NSError *error = nil;
    
    NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
    
    if (!error)
    {
        _success = [responseDictionary[@"success"] boolValue];
        
        NSDictionary *dataDictionary = responseDictionary[@"metadata"];
        
        NSArray *children = dataDictionary[@"results"];
        
        NSMutableArray *objects = [NSMutableArray array];
        
        for (NSDictionary *child in children)
        {
            NSString *itemId = child[kItemIdKey];
            
            Item *item = [[[DataManager sharedInstance] allForEntity:kItemEntity forFieldName:@"itemId" withFieldValue:itemId] lastObject];
            
            if (!item)
            {
                item = [[DataManager sharedInstance] createEntityWithName:kItemEntity];
            }
            
            [item updateWithInfo:child];
            
            [objects addObject:item];
        }
        
        [[DataManager sharedInstance] save];
        
        return objects;
    }
    else
    {
        _success = NO;
    }
    
    return nil;
}

@end
