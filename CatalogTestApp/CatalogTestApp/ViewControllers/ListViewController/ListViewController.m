//
//  ListViewController.m
//  CatalogTestApp
//
//  Created by Eugene Pavlyuk on 11/19/13.
//  Copyright (c) 2013 Eugene Pavlyuk. All rights reserved.
//

#import "ListViewController.h"
#import "SVPullToRefresh.h"
#import "CatalogModel.h"
#import "Item.h"
#import "UIImageView+WebCache.h"
#import "MBProgressHUD.h"

@implementation ItemTableViewCell


@end


@interface ListViewController ()

@property (nonatomic, weak) IBOutlet UITableView *mainTableView;
@property (nonatomic, strong) CatalogModel *catalogModel;

@end

@implementation ListViewController
{
    CatalogModel *catalogModel;
}

@synthesize catalogModel;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        self.catalogModel = [[CatalogModel alloc] init];
    }
    
    return self;
}

- (void)loadItems
{
    if (![self.catalogModel isLoading])
    {
        __weak ListViewController *weakSelf = self;
        
        NSMutableDictionary *info = [NSMutableDictionary dictionary];
        
        switch (segmentControl.selectedSegmentIndex) {
            case 0:
                [info setObject:@"popularity" forKey:kSortKey];
                break;
                
            case 1:
                [info setObject:@"name" forKey:kSortKey];
                break;
                
            case 2:
                [info setObject:@"price" forKey:kSortKey];
                break;
                
            case 3:
                [info setObject:@"brand" forKey:kSortKey];
                break;
                
            default:
                break;
        }
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [self.catalogModel loadItemsWithInfo:info
                              completionBlock:^{
                                  
                                  [weakSelf updateUI];
                                  
                                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                                  
                              } failureBlock:^{
                                  
                                  [weakSelf updateUI];
                                  
                                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                                  
                              }];
    }
}

- (IBAction)segmentControlValueChanged:(UISegmentedControl*)sender
{
    self.catalogModel.reloading = YES;
    [self loadItems];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Catalog";
    
    __weak ListViewController *weakSelf = self;
        
    [self.mainTableView addPullToRefreshWithActionHandler:^{
        weakSelf.catalogModel.reloading = YES;
        [weakSelf loadItems];
    }];
    
    [self.mainTableView addInfiniteScrollingWithActionHandler:^{
        [weakSelf loadItems];
    }];
    
    self.mainTableView.showsInfiniteScrolling = ([self.catalogModel.objects count] > 0);
    
    [self loadItems];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateUI
{
    [self.mainTableView reloadData];
    
    [self.mainTableView.pullToRefreshView stopAnimating];
    
    self.mainTableView.pullToRefreshView.lastUpdatedDate = [NSDate date];
    
    if (self.mainTableView.showsInfiniteScrolling)
    {
        [self.mainTableView.infiniteScrollingView stopAnimating];
    }
    
    self.mainTableView.showsInfiniteScrolling = [self.catalogModel canLoadMore];
}

#pragma mark - UITableViewDataSource's methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.catalogModel.objects count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cellIdentifier";
    
    ItemTableViewCell *itemTableViewCell = (ItemTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
    Item *item = self.catalogModel.objects[indexPath.row];
    
    itemTableViewCell.titleLabel.text = item.name;
    itemTableViewCell.descriptionLabel.text = [NSString stringWithFormat:@"%@ - %@", item.brand, item.price];
    
    NSURL *url = [NSURL URLWithString:item.image];
    
    [itemTableViewCell.spinnerView startAnimating];
    
    [itemTableViewCell.previewImageView setImageWithURL:url
                                                success:^(UIImage *image) {
                                                    
                                                    [itemTableViewCell.spinnerView stopAnimating];
                                                    
                                                    itemTableViewCell.previewImageView.image = image;
                                                    
                                                } failure:^(NSError *error) {
                                                    
                                                    [itemTableViewCell.spinnerView stopAnimating];
                                                    
                                                    itemTableViewCell.previewImageView.image = nil;
                                                    
                                                }];
    
    return itemTableViewCell;
}

@end

