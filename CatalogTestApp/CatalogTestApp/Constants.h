//
//  Constants.h
//  CatalogTestApp
//
//  Created by Eugene Pavlyuk on 11/19/13.
//  Copyright (c) 2013 Eugene Pavlyuk. All rights reserved.
//

#define kDefaultFetchLimit  @"15"
#define kDescKey               @"desc"
#define kBaseUrl            @"http://theiconic.bugfoot.de"
#define kCatalogUrl         @"mobile-api/womens-clothing"
#define kMaxitemsKey        @"maxitems"
#define kSortKey            @"sort"
#define kDirKey            @"dir"
#define kPageKey            @"page"
#define kItemIdKey          @"id"
#define kNameKey           @"name"
#define kDataKey            @"data"
#define kDataKey            @"data"
#define kImagesKey          @"images"
#define kPathKey            @"path"
#define kPriceKey           @"price"
#define kThumbnailKey       @"thumbnail"
#define kBrandKey           @"brand"