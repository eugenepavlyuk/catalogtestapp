//
//  DataManager.h
//  CatalogTestApp
//
//  Created by Eugene Pavlyuk on 11/19/13.
//  Copyright (c) 2011 Home. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User;

@interface DataManager : NSObject 
{
    
}

@property (nonatomic, readonly, retain) NSManagedObjectModel *objectModel;
@property (nonatomic, readonly, retain) NSManagedObjectContext *mainObjectContext;
@property (nonatomic, readonly, retain) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (DataManager*)sharedInstance;

@property (nonatomic, retain) User *user;

- (BOOL)save;

- (id)getEntityWithName:(NSString*)entityName;
- (id)createEntityWithName:(NSString*)entityName;

- (void)removeAllForEntity:(NSString *)entityName;

- (NSArray *)allRowsForEntity:(NSString *)entityName;
- (NSArray *)allForEntity:(NSString *)entityName forFieldName:(NSString *)fieldName withFieldValue:(NSString *)fieldValue;
- (NSArray *)allForEntity:(NSString *)entityName forFieldName1:(NSString *)fieldName1 withFieldValue1:(NSString *)fieldValue1 forFieldName2:(NSString *)fieldName2 withFieldValue2:(NSString *)fieldValue2;

@end
