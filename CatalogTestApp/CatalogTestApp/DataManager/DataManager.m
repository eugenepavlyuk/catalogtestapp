//
//  DataManager.m
//  CatalogTestApp
//
//  Created by Eugene Pavlyuk on 11/19/13.
//  Copyright (c) 2011 Home. All rights reserved.
//

#import "DataManager.h"

static DataManager *sharedInstance = nil;

@implementation DataManager

@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize mainObjectContext = _mainObjectContext;
@synthesize objectModel = _objectModel;

+ (DataManager*)sharedInstance
{
    if(sharedInstance)
    {
        return sharedInstance;
    }
    
    @synchronized(self)
    {
        if (!sharedInstance)
        {
            sharedInstance = [[self alloc] init];
        }
    }
    
	return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone
{
    if(sharedInstance)
    {
        return sharedInstance;
    }
    
    @synchronized(self)
    {
        if(!sharedInstance)
        {
            sharedInstance = [super allocWithZone:zone];
        }        
    }
    
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    
    if(self)
    {
        
    }
    
    return self;
}

- (void)dealloc 
{
	[self save];
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (id)getEntityWithName:(NSString*)entityName
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:self.mainObjectContext];
    [request setEntity:entity];
        
	NSArray *array = [self.mainObjectContext executeFetchRequest:request error:NULL]; 
    
    if ([array count])
    {
        return [array objectAtIndex:0];
    }
    else
    {
        NSManagedObject *object = [[DataManager sharedInstance] createEntityWithName:entityName];
                    
        [[DataManager sharedInstance] save];
        
        return object;
    }
    
    return nil;
}

- (void)removeAllForEntity:(NSString *)entityName
{
    NSArray *allEntities = [self allRowsForEntity:entityName];
    
    for (NSManagedObject *object in allEntities)
    {
        [self.mainObjectContext deleteObject:object];
    };
    
    [self save];
}

- (id)createEntityWithName:(NSString*)entityName
{
    id newObject = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:self.mainObjectContext];
    
    return newObject;
}

- (NSArray *)allForEntity:(NSString *)entityName forFieldName:(NSString *)fieldName withFieldValue:(NSString *)fieldValue
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:self.mainObjectContext];
    [request setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K like[cd] %@", fieldName, [NSString stringWithFormat:@"*%@*", fieldValue]];
    [request setPredicate:predicate];
    
	// execute
	return [self.mainObjectContext executeFetchRequest:request error:NULL];    
}

- (NSArray *)allForEntity:(NSString *)entityName forFieldName1:(NSString *)fieldName1 withFieldValue1:(NSString *)fieldValue1 forFieldName2:(NSString *)fieldName2 withFieldValue2:(NSString *)fieldValue2
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K like[cd] %@", fieldName1, [NSString stringWithFormat:@"*%@*", fieldValue1]];
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"%K like[cd] %@", fieldName2, [NSString stringWithFormat:@"*%@*", fieldValue2]];

    NSPredicate *compoundPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:[NSArray arrayWithObjects:predicate, predicate2, nil]];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:self.mainObjectContext];
    [request setEntity:entity];
    
    [request setPredicate:compoundPredicate];
    
	// execute
	return [self.mainObjectContext executeFetchRequest:request error:NULL];    
}

- (NSArray *)allRowsForEntity:(NSString *)entityName
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:self.mainObjectContext];
    [request setEntity:entity];
        
	// execute
	return [self.mainObjectContext executeFetchRequest:request error:NULL];
}

- (NSManagedObjectModel*)objectModel 
{
	if (_objectModel)
    {
		return _objectModel;
    }

	NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"database" withExtension:@"momd"];
	_objectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
	return _objectModel;
}

- (NSPersistentStoreCoordinator*)persistentStoreCoordinator
{
	if (_persistentStoreCoordinator)
    {
		return _persistentStoreCoordinator;
    }
    
	// Get the paths to the SQLite file
	NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"database.sqlite"];
    
	// Define the Core Data version migration options
	NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption,
                             nil];
    
	// Attempt to load the persistent store
	NSError *error = nil;
	_persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.objectModel];
	if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                   configuration:nil
                                                             URL:storeURL
                                                         options:options
                                                           error:&error]) {
		NSLog(@"Fatal error while creating persistent store: %@", error);
		abort();
	}
    
	return _persistentStoreCoordinator;
}

- (NSManagedObjectContext*)mainObjectContext 
{
	if (_mainObjectContext)
    {
		return _mainObjectContext;
    }
    
	// Create the main context only on the main thread
	if (![NSThread isMainThread]) 
    {
		[self performSelectorOnMainThread:@selector(mainObjectContext)
                               withObject:nil
                            waitUntilDone:YES];
		return _mainObjectContext;
	}
    
	_mainObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
	[_mainObjectContext setPersistentStoreCoordinator:self.persistentStoreCoordinator];
    
	return _mainObjectContext;
}

- (BOOL)save 
{
	if (![self.mainObjectContext hasChanges])
    {
		return YES;
    }
    
	NSError *error = nil;
    
	if (![self.mainObjectContext save:&error]) 
    {
		NSLog(@"Error while saving: %@\n%@", [error localizedDescription], [error userInfo]);
		
		return NO;
	}
    
	return YES;
}

@end